#!/bin/bash

#Path of GitLab server hooks
HOOK_PATH="/opt/gitlab/embedded/service/gitlab-shell/hooks"

PRE_RECEIVE_DIR="$HOOK_PATH/pre-receive.d"

#Path of GitLab user's home
GITLAB_HOME="/var/opt/gitlab"

SUCCESS=0
ERROR=1

#return value (successfully by default)
exit=$SUCCESS

#Check if GitLab Hooks directory exists
if ! [[ -d "$HOOK_PATH" ]] || ! [[ -d $GITLAB_HOME ]]; then
    echo "Error: $HOOK_PATH or $GITLAB_HOME not found"

    #exit with error
    exit=$ERROR
else
        #If GitLab Hooks directory exists, check if exists the pre-receive hook folder
        if ! [[ -d "$PRE_RECEIVE_DIR" ]];
        then
            #if not exists, create It
            mkdir "$PRE_RECEIVE_DIR"
        fi    

        #if exists, copy all files

        #copy pre-receive hook script in pre-receive.d folder
        cp pre-receive.sh "$PRE_RECEIVE_DIR"

        #add execution permissions to the script
        chmod +x "$PRE_RECEIVE_DIR/pre-receive.sh"

        #copy whitelist and limits configuration files to GitLab Home folder
        cp whitelist.txt limits.txt $GITLAB_HOME   
fi

#return exit value
exit $exit

