#!/bin/bash

LIMITS_FILE="$HOME/limits.txt"

#import sizelimits from file
source $LIMITS_FILE

#The repository size includes current file size
reposize_b=$(du -sb $PWD | cut -f 1)

#Show size in human-readable format
reposize_hr=$(du -sh $PWD | cut -f 1)

#get repository path (in format owner/repo_name) from git config file
repo_path=$(awk '/fullpath\s=\s/ {print $3}' "$PWD/config")

#check if this repository is in whitelist
is_vip=$(grep -w "$repo_path" "$HOME/whitelist.txt" | wc -l)

#recalculate size_limit in bytes
#if the repository is in whitelist, use VIP profile limits
if [ $is_vip -gt 0 ]; then
    sizelimit_b=$((sizelimit_vip*1024*1024))
    sizelimit_str="$sizelimit_vip MB"
#if the repository is not in whitelist, use basic profile limits
else
    sizelimit_b=$((sizelimit_basic*1024*1024))
    sizelimit_str="$sizelimit_basic MB"
fi


#check storage limits and show error if exceed
if [ $reposize_b -gt $sizelimit_b ]; then
    echo "Error: repository size $reposize_hr > $sizelimit_str"
    exit 1
else
    echo "Current size: $reposize_hr <= $sizelimit_str (size limit)"
fi

exit 0

