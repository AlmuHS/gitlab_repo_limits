#!/bin/bash

INSTALL=1
CHANGE_LIMITS=2
ADD_WHITELIST=3
RM_WHITELIST=4
UNINSTALL=5

#Path of GitLab user's home
GITLAB_HOME="/var/opt/gitlab"
HOOK_PATH="/opt/gitlab/embedded/service/gitlab-shell/hooks"
PRE_RECEIVE_DIR="$HOOK_PATH/pre-receive.d"

MENU=("Install tools" "Change limits" "Add repository to whitelist" "Remove repository from whitelist" "Uninstall tools")

source limits.txt

function change_limits(){

	read -rp "Enter basic size limit (in MB): " basic_sizelimit

	while [ "$basic_sizelimit" = "" ] || [ "$basic_sizelimit" -lt 0 ];
	do
		echo "Error: size must be >= 0"
		read -rp "Enter basic size limit (in MB): " basic_sizelimit
	done

	read -rp "Enter vip size limit (in MB): " vip_sizelimit

	while [ "$vip_sizelimit" = "" ] || [ "$vip_sizelimit" -lt 0 ];
	do
		echo "Error: size must be >= 0"
		read -rp "Enter vip size limit (in MB): " vip_sizelimit
	done

	#override previous values
	echo "sizelimit_basic=$basic_sizelimit" > limits.txt
	echo "sizelimit_vip=$vip_sizelimit" >> limits.txt
	
}

function show_limits(){
	source limits.txt

	echo "Current limits values are: "
	
	echo "basic size_limit: $sizelimit_basic MB"
	echo "vip size_limit: $sizelimit_vip MB"
}


function check_root(){
	if [[ $EUID -ne 0 ]]; then
		echo "This script must be run as root" 
		exit 1
	fi
}

function check_installation(){
	if ! [[ -d $PRE_RECEIVE_DIR ]];
	then
		echo "Error: Hook is not installed!!"
		exit 1
	fi
}

function show_menu(){
	menu_lenght=${#MENU[@]}

	for (( i=0; i<$menu_lenght; i++ )); 
	do
		echo "$((i+1)). ${MENU[$i]}";
	done
}

function install_tool(){
	show_limits
	read -rp "Do you want to change this values? (Y/[N])" change

	#if the user doesn't input any value, set "N" value by basic
	change=${change:-N}
	
	while [ "$change" = "Y" ]; do
		change_limits
		show_limits
		read -rp "Do you want to change this values? (Y/[N])" change
	done

	echo "Installing hook..."
	
	#run installer
	chmod +x installer.sh
	./installer.sh

	#check if installation finished successfully (0=correct, 1=error)
	return_code=$?

	#show advice about installation results
	if [ $return_code -eq 0 ];
	then
		echo "The installation finished successfully"
	else
		echo "Error: installation failed"
	fi
}

repo_path=""
function ask_repopath(){
	read -rp "Enter repository name: " repo_name
	read -rp "Enter repository owner (user or group): " repo_owner

	repo_path="$repo_owner/$repo_name"
}



echo "GitLab Storage Limits Hook"
echo "--------------------------"

check_root

echo "This is the configuration menu for GitLab Storage Limits"
echo "Select the action what you want: "

#show menu options
show_menu

read -rp "Option: " option
option=${option:-1}
echo "The option selected is ${MENU[$((option-1))]}"

case $option in
	$INSTALL )
		install_tool
	;;
	$CHANGE_LIMITS )
		check_installation
		change_limits
		show_limits
		cp limits.txt "$GITLAB_HOME"
	;;

	$ADD_WHITELIST )
		check_installation
		ask_repopath

		#add repository path to whitelist file
		echo "$repo_path" >> "$GITLAB_HOME/whitelist.txt"
	;;

	$RM_WHITELIST )
		check_installation
		ask_repopath

		#remove repository path from whitelist file
		sed -i "\|$repo_path$|d" "$GITLAB_HOME/whitelist.txt"
	;;

	$UNINSTALL )
		#keep pre-receive.d repository to preserve another pre-receive scripts
		rm -r "$PRE_RECEIVE_DIR/pre-receive.sh"
		rm "$GITLAB_HOME/limits.txt"
		rm "$GITLAB_HOME/whitelist.txt"
	;;
esac		
