# GitLab storage limits hook

## Introduction

This script set storage limits in a GitLab repository using [GitLab server-side hooks](https://docs.gitlab.com/ee/administration/server_hooks.html#set-a-global-server-hook-for-all-repositories)

The installer add a [pre-receive hook](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) to all repositories, which locks the push in case the repository exceed the limit size. 

The measure of repository size includes the size of the files added to the push

## Requirements

- GNU/Linux
- Bash
	+ awk
- Git
- GitLab (omnibus installation)

## Installation

This software must be installed inside the GitLab server

- **First option: Download sources using Git**

	  git clone https://gitlab.com/AlmuHS/gitlab_repo_limits.git

- **Second option: Download from the web**

	Download from GitLab Web and extract
	
	![*download sources from the web*](images/download_sources.png)

- **Install**

	To install It, simply run [`installer.sh`](https://gitlab.com/AlmuHS/gitlab_repo_limits/blob/master/installer.sh)  as root from a command line

	  # cd gitlab_repo_limits
	  # ./installer.sh


### Configuration Menu

This tool includes a text-mode menu to ease the installation and configuration.
This menu can be executed from command line, executing `menu.sh` as root

	# ./menu.sh

The menu will shows a interface like this:

	GitLab Storage Limits Hook
	--------------------------
	This is the configuration menu for GitLab Storage Limits
	Select the action what you want: 
	1. Install tools
	2. Change limits
	3. Add repository to whitelist
	4. Remove repository from whitelist
	5. Uninstall tools
	Option: 
	
The menu offers these options:

- **Install tools:** Install the pre-receive hook. Before the installation, this will shows the current sizelimit, and It will offer us to change these.

		Option: 1
		The option selected is Install tools
		Current limits values are: 
		basic size_limit: 100 MB
		vip size_limit: 2048 MB
		Do you want to change this values? (Y/[N])Y
		

	- If we press 'Y', we can change these limits. After change the values, the menu will shows them again, and It will repeat the question to change the values. **These questions will be repeated until press 'N'**
	
	- If we press 'N' (or simply press enter), It keep the current limits, and It will starts the installation

			Enter basic size limit (in MB): 100
			Enter vip size limit (in MB): 2000
			Current limits values are: 
			basic size_limit: 100 MB
			vip size_limit: 2000 MB
			Do you want to change this values? (Y/[N])N
			Installing hook...
		
- **Change limits**: Allow change sizelimits. **This option only can be executed once the tools are installed**. It will shows some question to change limits and, after change these, **It will override** `limits.txt` file (stored in `/var/opt/gitlab`) with the new values

- **Add repository to whitelist**:  Add a new repository to whitelist, **once application is installed.** It will ask the owner and name of the repository, and It will add the repository path to the whitelist.

- **Remove repository from whitelist**: Remove a repository from the whitelist **once application is installed.**

- **Uninstall application**: Remove all files: script, limits and whitelist. The pre-receive.d directory is preserved, to avoid removes another pre-receive scripts


## How It works

This software set a pre-receive hook to all repositories, which reject the push in case that this exceed the repository size limit

- **_pre-receive_ script**

	When a user executes a push action over a GitLab repository (using `git push` or uploading a file via GitLab web interface.), the *pre_receive* script is called.

	The _pre-receive_ hook is a bash script, `pre-receive.sh`, which measure the repository size executing `du -s` command over the repository folder in the server. **NOTE: This measure includes the size of the files uploaded in the push action**
	
	This size is compared with the sizelimits, which must be set in MB.
		
	If the repository exceed this size, the script return 1 to reject the request, showing by screen an error to advice the user about the problem, with the repository size and the sizelimit value. In other case, the script shows the current repository size, and accept the push
	
- **Limits**

	The sizelimits are set in `limits.txt`, stored in GitLab's home directory (`/var/opt/gitlab`).
	
	There are two limits profiles:
	- **Basic profile**: Default profile, with a limit of 50 MB
	- **VIP profile**: To bigger repositories, with a limit of 1024 MB
	
	The file has this format:
	
		#limit by default
		sizelimit_basic=50
		
		#used to the repositories added to whitelist
		sizelimit_vip=1024
		
	
	
	The script will read sizelimits from `limits.txt` file,  and It will set the sizelimit using the values stored in It, as this way:
	
	- By default, all repositories have a basic profile. To apply VIP profile to a repository, we can to add the repository path to the whitelist, stored in `whitelist.txt`  
	
		We can add a repository to whitelist using the repository path, with this format:
		
		- **Common repositories**
		
				owner_name/repository_name
				
		- **Group repositories**
		
				group_name/repository_name
				
		- **Subgroup repositories**
		
				group_name/subgroup_name/repository_name
				
	- When the script will be executed, this will check if the current repository path is in the whitelist. 
		- If yes, It will apply the VIP sizelimit stored in`sizelimit_vip`. 
		- In other case, It will apply the basic sizelimit from `basic_sizelimit`
	
	
	
	
	
	
	
	


	